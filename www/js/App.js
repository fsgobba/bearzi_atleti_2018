/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 17 2018
 *  File : App.js
 *******************************************/
class App {

    constructor() {
        console.log("new App");
        // inizializzazione delle proprietà di istanza
        this.gara = null;
        this._stato = null;

        // attendiamo che il documento sia caricato
        $(document).ready(() => {
            this._init();
        });
    }

    /**
     * funzione di inizializzazione generale
     */
    _init() {
        // creo la gara
        this.gara = new Gara(50);
        this.gara.generaAtleti(4);
        // popolo la pagina gara
        this.stampaAtleti();
        // imposto lo stato iniziale
        this._setStato("home");
        // TODO: togliere
        this._setStato("gara");
        // collego i pulsanti
        this._initPulsanti();
    }
    
    _initPulsanti() {
        $("#stato-home #crea").bind("click","crea-gara",(evento)=>this._clickHandler(evento));
        $("#stato-gara #parti").bind("click","parti",(evento)=>this._clickHandler(evento));
    }

    /**
     * Gestisce tutti i click dell'applicazione
     * @param {Event} evento 
     */
    _clickHandler(evento) {
        switch (evento.data) {
            case "crea-gara":
                this._setStato("gara");
                break;
            case "parti":
                /** richiama la funzione partenza di gara, 
                 * passadogli i due callback per update e risultato 
                 * */
                this.gara.partenza(()=>this._aggiornaRisultato(),()=>this.stampaRisultato());
                break;
            default:
                alert("click non previsto");
                break;
        }
    }

    _aggiornaRisultato() {
        // salvo l'array in una variabile locale per comodità
        var atleti = this.gara.gara_vo.atleti;
        for (var i = 0; i<atleti.length; i++) {
            var atleta = atleti[i];
            // calcolo la percentuale di avanzamento, arrotondata ad una cifra decimale
            var perc = Math.round((atleta.distanzaPercorsa/this.gara.gara_vo.distanza)*1000)/10;
            // recupero il div dell'atleta
            var div_atleta = $("#atleta-"+atleta.id);
            // recupero il div della barra
            var barra = div_atleta.find(".barra");
            // aggiorno la larghezza della barra
            barra.css("width",perc+"%");
        }
    }

    /**
     * sceglie la pagina da visualizzare
     * @param {String} stato 
     */
    _setStato(stato) {
        // aggiorno la variabile d'istanza
        this._stato = stato;
        var id_div;
        // scelgo il div della pagina da visualizzare
        switch (stato) {
            case "home":
                id_div = "stato-home";
                break;
            case "gara":
                id_div = "stato-gara";
                break;
        }
        // nascondiamo tutti gli stati
        $(".stato").css("display","none");
        // visualizzamo lo stato selezionato
        if (id_div) $("#"+id_div).css("display","block");
    }

    /**
     * utilizza GaraVO per rappresentare i 
     * div degli atleti
     */
    stampaAtleti() {
        // popolare la durata
        // creare i div degli atleti
        var template = $("#template-atleta").html();
        var contenitore = $("#atleti");
        for (var i=0; i<this.gara.gara_vo.atleti.length; i++) {
            var atleta = this.gara.gara_vo.atleti[i];
            var html = template.replace("***nome***", atleta.nome);
            html = html.replace("***eta***", atleta.eta);
            html = html.replace("***sesso***", atleta.sesso);
            html = html.replace("***id***", atleta.id);
            contenitore.append(html);
        }
    }

    stampaRisultato(gara) {

    }
}